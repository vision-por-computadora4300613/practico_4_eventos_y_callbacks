# Practico_4_Eventos_y_callbacks

Escribir un programa que permita seleccionar una porción rectangular de una imagen, luego:

- Con la letra “g” guardar la porción de la imagen seleccionada como una nueva imagen.

- Con la letra “r” restaurar la imagen original y permitir realizar una nueva selección.

- Con la “q” finalizar.
    

## How to use it

Para correr el script, pararse sobre la carpeta del proyecto y en la terminal escribir:

    python3 evyca.py

La imágen que se utiliza es la del DIBU. La imágen recortada se guarda en cropped.jpg.
