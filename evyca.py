# %% [markdown]
# ---------------------------------------------------------------------------------------------------
# ## Script que selcciona una parte de una imagen
# 
# 
# Selecciona una porción rectangular de una imagen: 
# - Con la letra “g” guardar la porción de la imagen seleccionada como una nueva imagen.
# - Con la letra “r” restaurar la imagen original y permitir realizar una nueva selección.
# - Con la “q” finalizar.
# ---------------------------------------------------------------------------------------------------

# %% [markdown]
# Importamos La librería y declaramos las variables:
# 
#     - original: nunca cambia.
#     - clone: es la que está cambiando todo el tiempo.
#     - cropped: es la va acortándose. 
#     - buffer: es la que se va a guardar cuando soltemos el click.

# %%
#! /usr/bien/env python
# -*- coding: utf-8-*-

import cv2 

drawing = False     #true if mouse is pressed
ix, iy = -1, -1

original = cv2.imread("dibu.jpg", cv2.IMREAD_COLOR)
clone = original.copy()
cropped = original.copy()
buffer = original.copy()


# %% [markdown]
# *draw_rectangle* para dibujar un rectángulo.
# 
# Esta es la función callback. Esta si o si debe tener la siguiente firma: onMouse(event, x, y, flags, param).
# 
# Aunque en este caso específico no se utilicen los parámetros flags y param, es importante mantenerlos en la firma de la función draw_circle porque la función cv2.setMouseCallback espera que la función callback tenga esa firma específica.
# 
# img, clone y original tienen que estar como globales para que se entienda que son las que declaramos antes.

# %%

def draw_rectangle(event, x, y, flags, param):
    global ix, iy, drawing, cropped, clone, buffer
    

    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True      
        ix, iy = x, y
        clone = cropped.copy()      #Para actualizar la imagen y que no se superpongan los rectángulos

        
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        clone = cropped.copy()       #Para actualizar la imagen
        cv2.rectangle(clone, (ix, iy), (x, y), (0, 255, 0), 3)      ##cv2.rectangle(img, pto1, pto2, color, grosor, tipo de linea)

        if ix<x and iy<y:        # Para poder hacer el rectángulo de izquierda a derecha o viceversa y de abajo para arriba o viceversa.
            buffer = cropped[iy:y, ix:x]
        elif ix<x and iy>y:
            buffer = cropped[y:iy, ix:x]
        elif ix>x and iy<y:
            buffer = cropped[iy:y, x:ix]
        else:                               #ix>x and iy>y
            buffer = cropped[y:iy, x:ix]
        
        
        
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing is True:
            clone = cropped.copy()   #Para actualizar la imagen
            cv2.rectangle(clone, (ix, iy), (x, y), (0, 255, 0), 3)


# %% [markdown]
# 
# *cv2.namedWindow:* crea una ventana con un nombre y tamaño modificable para mostrar videos o imagenes que les pasemos por parámetro.
# 
# *cv2.setMouseCallback:* 1er parámetro, donde se va a fijar si hubo un evento. 2do parámetro, que va a hacer.

# %%

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_rectangle) 


# %% [markdown]
# LLamamos constantemente a la función imshow para mostrar la imagen. 
# 
#     . Con 'r' restauramos.
#     . Con 'g' guardamos.
#     . Con 'q' salimos.
# Luego destruimos todas las ventanas con destroyAllWindows().

# %%

while(1):
    cv2.imshow('image', clone)
    k = cv2.waitKey(1) & 0xFF

    if k == ord('r'):
        cropped = original.copy()
        clone = original.copy()
        
    elif k == ord('g'):
        cropped = buffer.copy()
        clone = cropped.copy()
        cv2.imwrite("cropped.jpg", buffer)
        
    elif k == ord('q'):
        break

    
cv2.destroyAllWindows()
 


